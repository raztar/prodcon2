#include <QCoreApplication>
#include "../shm_queue.h"
#include "NanoLogCpp17.h"
#include <unistd.h>

using namespace NanoLog::LogLevels;
using namespace NanoLog;

using namespace std;
using namespace std::chrono;


int main(int argc, char *argv[])
{
    map_thread_to_cpu(1);
    cout << "Message Id,Time(ns)" << endl;
////     Optional: Set the output location for the NanoLog system. By default
////     the log will be output to ./compressedLog

//    NanoLog::setLogFile("/tmp/consumerQt");

//    //    // Optional optimization: pre-allocates thread-local data structures
//    //    // needed by NanoLog. This can be invoked once per new
//    //    // thread that will use the NanoLog system.
//    //    NanoLog::preallocate();

//    // Optional: Set the minimum LogLevel that log messages must have to be
//    // persisted. Valid from least to greatest values are
//    // DEBUG, NOTICE, WARNING, ERROR
//    NanoLog::setLogLevel(NOTICE);
//    NANO_LOG(NOTICE,",Message,Time(ns)\n");

    shm_queue<packet> testQueue(1000, 1000);

    while(true)
    {
        packet auxPacket;
        if (testQueue.dequeue(auxPacket))
        {
            int x = duration_cast<nanoseconds>(high_resolution_clock::now() - auxPacket.tp).count();
            //NANO_LOG(NOTICE,",%d,%d\n", auxPacket.value, x);
            cout << auxPacket.value << "," << to_string(x) << endl;
        }
    }
}
